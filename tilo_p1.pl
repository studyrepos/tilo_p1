% facts
parent(trudi, emma).
parent(siggi, emma).
parent(lisa, siggi).
parent(fritz, siggi).
parent(lisa, ralf).
parent(fritz, ralf).
parent(peter, rudi).
parent(hilde, rudi).
parent(peter, sophie).
parent(hilde, sophie).
parent(hans, peter).
parent(gerda, peter).
parent(hans, lisa).
parent(gerda, lisa).

m(hans).
m(peter).
m(rudi).
m(fritz).
m(ralf).
m(siggi).
f(gerda).
f(hilde).
f(sophie).
f(lisa).
f(trudi).
f(emma).

% relations
father(Father, Child) :- 
    parent(Father, Child), 
    m(Father).

mother(Mother, Child) :- 
    parent(Mother, Child), 
    f(Mother).

son(Son, Parent) :- 
    parent(Parent, Son),
    m(Son).

daughter(Daughter, Parent) :- 
    parent(Parent, Daughter), 
    f(Daughter).

% brother/sister/siblings and child have both same mother and father, but are not the same person
brother(Brother, Child) :- 
    father(Father, Child),     
    father(Father, Brother), 
    mother(Mother, Child), 
    mother(Mother, Brother), 
    m(Brother), 
    Child \== Brother.

sister(Sister, Child) :- 
    father(Father, Child), 
    father(Father, Sister),
    mother(Mother, Child), 
    mother(Mother, Sister), 
    f(Sister), 
    Child \== Sister.

siblings(Child1, Child2) :- 
    mother(Mother, Child1), 
    mother(Mother, Child2),
    father(Father, Child1), 
    father(Father, Child2), 
    Child1 \== Child2.

% uncle/aunt is brother/sister of parent of child
uncle(Uncle, Child) :-  
    parent(Parent, Child), 
    brother(Uncle, Parent).

aunt(Aunt, Child) :- 
    parent(Parent, Child), 
    sister(Aunt, Parent).

% cousine/cousin is daughter/son of siblings of childs parents
cousine(Cousine, Child) :-
    daughter(Cousine, Parent1),
    parent(Parent2, Child), 
    siblings(Parent1, Parent2), 
    Cousine \== Child.

cousin(Cousin, Child) :- 
    son(Cousin, Parent1),
    parent(Parent2, Child), 
    siblings(Parent1, Parent2), 
    Cousin \== Child.

grandchild(Gchild, Gparent) :- 
    parent(Gparent, Parent), 
    parent(Parent, Gchild).
grandparent(Gparent, Gchild) :- 
    grandchild(Gchild, Gparent).

grandfather(Gfather, Gchild) :- 
    grandparent(Gfather, Gchild), m(Gfather).

grandmother(Gmother, Gchild) :- 
    grandparent(Gmother, Gchild), f(Gmother).
